# async_ml

The goal of this project is to explore the capabilities of running async requests to a ML project - tools investigated here include Celery, Redis, RabbitMQ, and Seldon.

Setup requires Docker and Minikube to run.

## Basic Celery
Getting started with the basics of celery and running a few examples.

All of the examples run from the "demo" folder
1. ```docker run -d -p 5672:5672 rabbitmq```
2. ```docker run -d -p 6379:6379 redis```
3. Making sure you are in the right directory (demo), run ```celery -A tasks worker --loglevel=INFO```
4. Run `runme.py` as a sample script to test out functionality

## Seldon Example

1. Instructions are found [here](https://github.com/wmaucla/seldon_experiments)
2. Sample curl request is as follows: 
```
curl  -s http://localhost:8003/seldon/seldon/seldon-deployment-example/api/v0.1/predictions -H "Content-Type: application/json" -d '{"data":{"ndarray":[[5.964,4.006,2.081,1.031]]}}
```
