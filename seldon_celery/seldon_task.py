from celery import Celery
import numpy as np
from seldon_core.seldon_client import SeldonClient

app = Celery('seldon_task', backend='redis://localhost', broker='pyamqp://')

@app.task
def seldon_invoke(a, b, c, d):
    sc = SeldonClient(deployment_name="seldon-deployment-example", namespace="seldon",
                      gateway_endpoint="localhost:8003")
    r = sc.predict(transport="rest", data=np.array([[a, b, c, d]]), client_return_type="dict")
    print(r.response)
